<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <style>
        .form-control:focus{
            box-shadow: none!important;
           
        }
    </style>
</head>

<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6 ">
                <div class="card ">
                    <div class="card-header ">
                        <h3 class="text-center  text-dark p-2">Registration Form</h3>
                        <div class="card-body ">
                            <form>
                                <div class="row g-3 mb-3">
                                    <div class="col">
                                        <label for="exampleInputFirstName" class="form-label text-dark">First
                                            Name</label>
                                        <input type="text" class="form-control" placeholder="First name">
                                    </div>
                                    <div class="col">
                                        <label for="exampleInputFirstName" class="form-label text-dark">Last
                                            Name</label>
                                        <input type="text" class="form-control" placeholder="Last name">
                                    </div>
                                </div>
                                <div class="row g-3 mb-3">
                                    <div class="col">
                                        <label for="exampleInputEmail" class="form-label text-dark">Email</label>
                                        <input type="text" class="form-control" placeholder="Email" aria-label="First name">
                                    </div>
                                    <div class="col">
                                        <label for="exampleInputPhoneNumber" class="form-label text-dark">Phone
                                            Number</label>
                                        <input type="number" class="form-control" placeholder="Phone Number" aria-label="Last name">
                                    </div>
                                </div>
                                <div class="row g-3 mb-3">
                                    <div class="col">
                                        <label for="exampleInputPassword" class="form-label text-dark">Password</label>
                                        <input type="password" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="col">
                                        <label for="exampleInputGender" class="form-label text-dark">Gender</label>
                                        <select id="inputGender" class="form-select">
                                            <option selected>Choose...</option>
                                            <option>Male</option>
                                            <option>Female</option>
                                            <option>Others</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="row g-3 mb-3">
                                    <div class="col">
                                        <label for="exampleInputEmail" class="form-label text-dark">Address</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="2"></textarea>
                                    </div>

                                </div>
                                <div class="row g-3 mb-3">
                                    <div class="col-4">
                                        <label for="exampleInputDistrict" class="form-label text-darkt">District</label>
                                        <select id="inputDistrict" class="form-select">
                                            <option selected>Choose...</option>
                                            <option>Erode</option>
                                            <option>Coimbatore</option>
                                            <option>Chennai</option>
                                        </select>

                                    </div>
                                    <div class="col-4">
                                        <label for="exampleInputState" class="form-label text-dark">State</label>
                                        <select id="inputState" class="form-select">
                                            <option selected>Choose...</option>
                                            <option>Tamilnadu</option>
                                            <option>Kerala</option>
                                            <option>Karnataka</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <label for="exampleInputZip" class="form-label text-dark">Zip</label>
                                        <input type="text" class="form-control" placeholder="Zip" aria-label="First name">
                                    </div>
                                </div>

                                <div class="row g-3 mb-3">
                                    <div class="col ">
                                        <button type="submit" class="form-control bg-primary text-light mt-2">Register</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>